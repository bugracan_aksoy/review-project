package com.andersenlab.review.controller;

import com.andersenlab.review.model.Restaurant;
import com.andersenlab.review.request.RestaurantRatingUpdateRequest;
import com.andersenlab.review.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/* bugracanaksoy created on 14.04.2023 */
@RestController
@RequestMapping("/restaurant")
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    private static final Logger logger = Logger.getLogger(RestaurantController.class.getName());

    // Adding a new restaurant
    @PostMapping
    public ResponseEntity<Restaurant> addRestaurant(@RequestBody Restaurant restaurant) {
        logger.info("Received a request to add a new restaurant");
        Restaurant createdRestaurant = restaurantService.addRestaurant(restaurant);
        logger.info("Added a new restaurant with id: " + createdRestaurant.getId());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdRestaurant);
    }

    // Updating restaurant rating
    @PatchMapping("/{id}")
    public ResponseEntity<Restaurant> updateRestaurantRating(@PathVariable int id, @RequestBody RestaurantRatingUpdateRequest request) {
        logger.info("Received a request to update rating of a restaurant with id: " + id);
        Restaurant updatedRestaurant = restaurantService.updateRestaurantRating(id, request.getAverageRating(), request.getVotes());
        logger.info("Updated rating of the restaurant with id: " + id);
        return ResponseEntity.ok(updatedRestaurant);
    }

    // Getting all restaurants
    @GetMapping
    public ResponseEntity<List<Restaurant>> getAllRestaurants(@RequestParam(defaultValue = "0") int page,
                                                              @RequestParam(defaultValue = "10") int size) {
        logger.info("Received a request to get all restaurants");
        List<Restaurant> restaurants = restaurantService.getAllRestaurants(PageRequest.of(page, size));
        logger.info("Returning " + restaurants.size() + " restaurants on page " + page);
        return ResponseEntity.ok(restaurants);
    }

    // Getting all restaurants in a particular city
    @GetMapping(value = "/query", params = "city")
    public ResponseEntity<List<Restaurant>> getRestaurantsByCity(@RequestParam(defaultValue = "0") int page,
                                                                 @RequestParam(defaultValue = "10") int size,
                                                                 @RequestParam String city) {
        logger.info("Received a request to get restaurants by city: " + city);
        List<Restaurant> restaurants = restaurantService.getRestaurantsByCity(PageRequest.of(page, size),city);
        logger.info("Returning " + restaurants.size() + " restaurants in the city: " + city);
        return ResponseEntity.ok(restaurants);
    }

    @GetMapping(value = "/query", params = "id")
    public ResponseEntity<Restaurant> getRestaurantById(@RequestParam int id) {
        logger.info("Received a request to get restaurant by id: " + id);
        Restaurant restaurant = restaurantService.getRestaurantById(id);
        logger.info("Returning the restaurant with id: " + id);
        return ResponseEntity.ok(restaurant);
    }

    // Deleting restaurant by id
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRestaurantById(@PathVariable int id) {
        logger.info("Received a request to delete restaurant with id: " + id);
        restaurantService.deleteRestaurant(id);
        logger.info("Deleted the restaurant with id: " + id);
        return ResponseEntity.noContent().build();
    }

    // Sort the restaurants according to rating
    @GetMapping("/sort")
    public ResponseEntity<List<Restaurant>> sortRestaurantsByRating(@RequestParam(defaultValue = "0") int page,
                                                                    @RequestParam(defaultValue = "10") int size) {
        logger.info("Received a request to sort the restaurants by rating");
        List<Restaurant> sortedRestaurants = restaurantService.sortRestaurantsByRating(PageRequest.of(page, size));
        logger.info("Returning " + sortedRestaurants.size() + " restaurants sorted by rating");
        return ResponseEntity.ok(sortedRestaurants);
    }
}
