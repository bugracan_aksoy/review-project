package com.andersenlab.review.request;

import lombok.Data;

/* bugracanaksoy created on 14.04.2023 */
@Data
public class RestaurantRatingUpdateRequest {
    private String averageRating;
    private int votes;
}
