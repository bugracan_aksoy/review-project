package com.andersenlab.review.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/* bugracanaksoy created on 14.04.2023 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "restaurants")
public class Restaurant {

    @Id
    private int id;
    private String city;
    private String name;
    private int estimatedCost;
    private String averageRating;
    private int votes;

}
