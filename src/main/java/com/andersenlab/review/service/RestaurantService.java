package com.andersenlab.review.service;/* bugracanaksoy created on 14.04.2023 */

import com.andersenlab.review.model.Restaurant;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface RestaurantService {
    Restaurant addRestaurant(Restaurant restaurant);
    Restaurant updateRestaurantRating(int id, String average, int votes);
    List<Restaurant> getAllRestaurants(PageRequest pageRequest);
    List<Restaurant> getRestaurantsByCity(PageRequest pageRequest,String city);
    Restaurant getRestaurantById(int id);
    void deleteRestaurant(int id);
    List<Restaurant> sortRestaurantsByRating(PageRequest pageRequest);
}
