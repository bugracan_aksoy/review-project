package com.andersenlab.review.service;

import com.andersenlab.review.model.Restaurant;
import com.andersenlab.review.util.Sequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

/* bugracanaksoy created on 14.04.2023 */
@Service
public class RestaurantServiceImpl implements RestaurantService {

    private static final Logger logger = LoggerFactory.getLogger(RestaurantServiceImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private Sequence sequence;

    @Override
    public Restaurant addRestaurant(Restaurant restaurant) {
        logger.info("Adding restaurant: {}", restaurant);
        synchronized (sequence) {
            restaurant.setId(sequence.nextValue());
        }
        return mongoTemplate.insert(restaurant);
    }

    @Override
    public Restaurant updateRestaurantRating(int id, String average, int votes) {
        logger.info("Updating restaurant with id {} with average rating {} and votes {}", id, average, votes);
        Restaurant restaurant = mongoTemplate.findById(id, Restaurant.class);
        if (restaurant != null) {
            restaurant.setAverageRating(average);
            restaurant.setVotes(votes);
            return mongoTemplate.save(restaurant);
        } else {
            logger.error("Restaurant with id {} not found", id);
            throw new NoSuchElementException("Restaurant not found");
        }
    }

    @Override
    public List<Restaurant> getAllRestaurants(PageRequest pageRequest) {
        logger.info("Getting all restaurants with page request: " + pageRequest);
        Pageable pageable = PageRequest.of(pageRequest.getPageNumber(), pageRequest.getPageSize());
        Query query = new Query();
        synchronized (this) {
            List<Restaurant> restaurants = mongoTemplate.find(query.with(pageable), Restaurant.class);
            return Collections.synchronizedList(restaurants);
        }
    }

    @Override
    public List<Restaurant> getRestaurantsByCity(PageRequest pageRequest, String city) {
        logger.info("Getting restaurants by city: {}", city);
        Query query = new Query(Criteria.where("city").is(city));
        query.with(pageRequest);
        synchronized (this) {
            List<Restaurant> restaurants = mongoTemplate.find(query, Restaurant.class);
            return Collections.synchronizedList(restaurants);
        }
    }

    @Override
    public Restaurant getRestaurantById(int id) {
        logger.info("Getting restaurant by id: {}", id);
        synchronized (this) {
            Restaurant restaurant = mongoTemplate.findById(id, Restaurant.class);
            if (restaurant != null) {
                return restaurant;
            } else {
                logger.error("Restaurant with id {} not found", id);
                throw new NoSuchElementException("Restaurant not found");
            }
        }
    }

    @Override
    public void deleteRestaurant(int id) {
        logger.info("Deleting restaurant with id: {}", id);
        synchronized (this) {
            Restaurant restaurant = mongoTemplate.findById(id, Restaurant.class);
            if (restaurant != null) {
                mongoTemplate.remove(restaurant);
            } else {
                logger.error("Restaurant with id {} not found", id);
                throw new NoSuchElementException("Restaurant not found");
            }
        }
    }

    @Override
    public List<Restaurant> sortRestaurantsByRating(PageRequest pageRequest) {
        logger.info("Sorting restaurants by rating");
        Aggregation aggregation = Aggregation.newAggregation(Aggregation.sort(Sort.Direction.DESC, "averageRating"), Aggregation.skip(pageRequest.getPageNumber() * pageRequest.getPageSize()), Aggregation.limit(pageRequest.getPageSize()));
        synchronized (this) {
            AggregationResults<Restaurant> results = mongoTemplate.aggregate(aggregation, "restaurants", Restaurant.class);
            return Collections.synchronizedList(results.getMappedResults());
        }
    }
}
