package com.andersenlab.review.util;

import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

/* bugracanaksoy created on 14.04.2023 */

@Service
public class Sequence {

    private static final AtomicInteger counter = new AtomicInteger();

    public synchronized static int nextValue() {
        return counter.getAndIncrement();
    }
}