package com.andersenlab.review.controller;

import com.andersenlab.review.model.Restaurant;
import com.andersenlab.review.request.RestaurantRatingUpdateRequest;
import com.andersenlab.review.service.RestaurantService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

class RestaurantControllerTest {
    @Mock
    private RestaurantService restaurantService;

    @InjectMocks
    private RestaurantController restaurantController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testAddRestaurant() {
        // Arrange
        Restaurant restaurant = new Restaurant();
        restaurant.setName("Test Restaurant");
        when(restaurantService.addRestaurant(restaurant)).thenReturn(restaurant);

        // Act
        ResponseEntity<Restaurant> responseEntity = restaurantController.addRestaurant(restaurant);

        // Assert
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(restaurant, responseEntity.getBody());
    }

    @Test
    public void testUpdateRestaurantRating() {
        // Arrange
        Restaurant restaurant = new Restaurant();
        restaurant.setId(1);
        restaurant.setName("Test Restaurant");
        RestaurantRatingUpdateRequest request = new RestaurantRatingUpdateRequest();
        request.setAverageRating(String.valueOf(4.5));
        request.setVotes(10);
        when(restaurantService.updateRestaurantRating(1, String.valueOf(4.5), 10)).thenReturn(restaurant);

        // Act
        ResponseEntity<Restaurant> responseEntity = restaurantController.updateRestaurantRating(1, request);

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(restaurant, responseEntity.getBody());
    }

    @Test
    public void testGetAllRestaurants() {
        // Arrange
        List<Restaurant> restaurants = Arrays.asList(new Restaurant(), new Restaurant());
        when(restaurantService.getAllRestaurants(PageRequest.of(0, 10))).thenReturn(restaurants);

        // Act
        ResponseEntity<List<Restaurant>> responseEntity = restaurantController.getAllRestaurants(0, 10);

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(restaurants, responseEntity.getBody());
    }

    @Test
    public void testGetRestaurantsByCity() {
        // Arrange
        List<Restaurant> restaurants = Arrays.asList(new Restaurant(1, "Ankara", "BurgerKing",51,"4.9",12321),
                new Restaurant(2, "Istanbul", "McDonalds",50,"4.2",5123),
                new Restaurant(3, "Izmir", "KFC",52,"4.1",215));
        when(restaurantService.getRestaurantsByCity(PageRequest.of(0, 10), "Ankara")).thenReturn(restaurants);

        // Act
        ResponseEntity<List<Restaurant>> responseEntity = restaurantController.getRestaurantsByCity(0, 10, "Ankara");

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(restaurants, responseEntity.getBody());
    }

    @Test
    public void testGetRestaurantById() {
        // Arrange
        Restaurant restaurant = new Restaurant();
        restaurant.setId(1);
        restaurant.setName("Test Restaurant");
        when(restaurantService.getRestaurantById(1)).thenReturn(restaurant);

        // Act
        ResponseEntity<Restaurant> responseEntity = restaurantController.getRestaurantById(1);

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(restaurant, responseEntity.getBody());
    }

    @Test
    public void deleteRestaurantById() {
        // Arrange
        int id = 1;
        doNothing().when(restaurantService).deleteRestaurant(id);

        // Act
        ResponseEntity<Void> response = restaurantController.deleteRestaurantById(id);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void sortRestaurantsByRating() {
        // Arrange
        int page = 0;
        int size = 10;
        List<Restaurant> sortedRestaurants = Arrays.asList(new Restaurant(1, "Ankara", "BurgerKing",51,"4.9",12321),
                new Restaurant(2, "Istanbul", "McDonalds",50,"4.2",5123),
                new Restaurant(3, "Izmir", "KFC",52,"4.1",215));
        PageRequest pageRequest = PageRequest.of(page, size);
        when(restaurantService.sortRestaurantsByRating(pageRequest)).thenReturn(sortedRestaurants);

        // Act
        ResponseEntity<List<Restaurant>> response = restaurantController.sortRestaurantsByRating(page, size);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(sortedRestaurants, response.getBody());
    }
}